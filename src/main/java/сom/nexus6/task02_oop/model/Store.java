package сom.nexus6.task02_oop.model;

import java.util.ArrayList;
import сom.nexus6.task02_oop.observer.Observable;
import сom.nexus6.task02_oop.prod.Product;
import сom.nexus6.task02_oop.servicepack.Filterable;
import сom.nexus6.task02_oop.servicepack.StoreGenerator;

public class Store implements Publishible {
  private ArrayList<Product> storeData;
  private ArrayList<Observable> observers = new ArrayList<Observable>();
  private ArrayList<Product> filteredData;
  private boolean state = false;

  public Store() {
    StoreGenerator gen = new StoreGenerator();
    storeData = gen.fillStrore();
  }

  public boolean isState() {
    return state;
  }

  public void setState(boolean state) {
    this.state = state;
  }

  public void registerObs(Observable obs) {
    observers.add(obs);
  }
  public void registerObs(Observable obs, Filterable fRule) {
    observers.add(obs);
    filterData(fRule);
    notifyObs();
  }
  public void removeObs(Observable obs) {
    int ind = observers.indexOf(obs);
    if (ind >= 0) {
      observers.remove(ind);
    }
    state = false;
  }
  private void notifyObs() {
    if (isState()) {
      for(Observable o :observers) {
        o.update(this);
      }
    }
  }
  private  <T extends  Filterable> void filterData(T filter) {
    ArrayList<Product> data = storeData;
    filteredData = filter.applyFilter(data);
  }
  public ArrayList<Product> getOutputData() {
    return filteredData;
  }
}
