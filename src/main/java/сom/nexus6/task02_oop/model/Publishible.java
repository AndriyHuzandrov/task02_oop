package сom.nexus6.task02_oop.model;

import java.util.ArrayList;
import сom.nexus6.task02_oop.observer.Observable;
import сom.nexus6.task02_oop.prod.Product;
import сom.nexus6.task02_oop.servicepack.Filterable;

public interface Publishible {

  void registerObs(Observable obs);

  void registerObs(Observable obs, Filterable fr);

  void removeObs(Observable obs);

  ArrayList<Product> getOutputData();
}
