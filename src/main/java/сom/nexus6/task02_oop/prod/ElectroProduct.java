package сom.nexus6.task02_oop.prod;

public abstract class ElectroProduct extends Product {
  private int voltage;

  ElectroProduct(String name) {
    super(name);
  }

  public int getVoltage() {
    return voltage;
  }
  public void setVoltage(int voltage) {
    this.voltage = voltage;
  }
}
