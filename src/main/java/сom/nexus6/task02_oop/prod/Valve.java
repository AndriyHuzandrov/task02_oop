package сom.nexus6.task02_oop.prod;

public class Valve extends PlumbingProduct {
  private boolean showwer;

  public Valve(String name) {
    super(name);
  }

  public boolean isShowwer() {
    return showwer;
  }

  public void setShowwer(boolean showwer) {
    this.showwer = showwer;
  }
}
