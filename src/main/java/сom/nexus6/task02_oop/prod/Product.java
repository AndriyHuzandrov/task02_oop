package сom.nexus6.task02_oop.prod;

public abstract class Product {
  protected double price;
  protected double weight;
  protected String name;
  protected String countryOfOrg;

  public Product(String name) {
    this.name = name;
  }

  public String getCountryOfOrg() {
    return countryOfOrg;
  }

  public void setCountryOfOrg(String countryOfOrg) {
    this.countryOfOrg = countryOfOrg;
  }

  public double getPrice() {
    return price;
  }

  public double getWeight() {
    return weight;
  }
  public String getName() {
    return name;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public void setWeight(double weight) {
    this.weight = weight;
  }

  public void setName(String name) {
    this.name = name;
  }
}
