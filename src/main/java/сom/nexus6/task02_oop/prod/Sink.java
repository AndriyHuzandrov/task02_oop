package сom.nexus6.task02_oop.prod;

public class Sink extends PlumbingProduct {
  private String type;

  public Sink(String name) {
    super(name);
  }

  public void setType(String type) {
    this.type = type;
  }
  public String getType() {
    return type;
  }
}
