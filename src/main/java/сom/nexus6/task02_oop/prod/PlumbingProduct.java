package сom.nexus6.task02_oop.prod;

public abstract class PlumbingProduct extends Product {
  private int couplingDiam;

  PlumbingProduct(String name) {
    super(name);
  }

  public int getCouplingDiam() {
    return couplingDiam;
  }
  public void setCouplingDiam(int couplingDiam) {
    this.couplingDiam = couplingDiam;
  }
}
