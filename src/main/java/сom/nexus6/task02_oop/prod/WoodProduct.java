package сom.nexus6.task02_oop.prod;

public abstract class WoodProduct extends Product {

  private int size;
  private String color;

  WoodProduct(String name) {
    super(name);
  }

  public int getSize() {
    return size;
  }
  public String getColor() {
    return color;
  }
  public void setSize(int size) {
    this.size = size;
  }
  public void setColor(String color) {
    this.color = color;
  }
}
