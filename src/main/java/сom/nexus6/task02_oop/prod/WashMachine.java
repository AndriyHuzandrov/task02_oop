package сom.nexus6.task02_oop.prod;

public class WashMachine extends PlumbingProduct {
  private String model;
  private int consPowwer;

  public WashMachine(String name) {
    super(name);
  }

  public String getModel() {
    return model;
  }
  public int getConsPowwer() {
    return consPowwer;
  }
  public void setModel(String model) {
    this.model = model;
  }
  public void setConsPowwer(int consPowwer) {
    this.consPowwer = consPowwer;
  }
}
