package сom.nexus6.task02_oop.prod;

 public class Switch extends ElectroProduct {
  private int button;

   public Switch(String name) {
     super(name);
   }

   public int getButton() {
    return button;
  }

  public void setButton(int button) {
    this.button = button;
  }
}
