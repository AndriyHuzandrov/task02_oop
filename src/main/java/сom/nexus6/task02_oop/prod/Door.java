package сom.nexus6.task02_oop.prod;

 public class Door extends WoodProduct {
  private String doorType;

   public Door(String name) {
    super(name);
   }

   public String getDoorType() {
    return doorType;
  }
  public void setDoorType(String doorType) {
    this.doorType = doorType;
  }
}
