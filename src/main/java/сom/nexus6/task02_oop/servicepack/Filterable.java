package сom.nexus6.task02_oop.servicepack;

import java.util.ArrayList;
import сom.nexus6.task02_oop.prod.Product;

public interface Filterable {

  ArrayList<Product> applyFilter(ArrayList<Product> data);

}
