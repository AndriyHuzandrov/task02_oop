package сom.nexus6.task02_oop.servicepack;

import java.util.ArrayList;
import java.util.Random;
import сom.nexus6.task02_oop.prod.Product;
import сom.nexus6.task02_oop.prod.*;
/**
 * Synthetic class
 *
 */
public class StoreGenerator {
  private Random rnd = new Random(50);
  private static  String[] contry = {"de", "ua", "ch"};
  private static String[] sinkType = {"metal", "porcelane", "plastec"};
  private static String[] prodName = {"alpha", "beta", "gamma"};
  private static String[] washMachModel = {"LG", "Samsung", "Bosch"};
  private static String[] floorText = {"oak", "beech", "pine"};
  private static String[] dType = {"entrance", "internal", "glass"};
  private static String[] colorArr = {"black", "brown", "grey"};

  private int rndArrInd() {
    return rnd.nextInt(3);
  }

  private <T extends Product> void fillSuperProp(T prod) {
    Random rnd = new Random();
    prod.setWeight(rnd.nextInt(10));
    prod.setPrice(rnd.nextDouble() * 100);
    prod.setCountryOfOrg(contry[rndArrInd()]);
  }
  private <T extends ElectroProduct> void fillElectroProp(T prod) {
    Random rnd = new Random(50);
    prod.setVoltage(220 + rnd.nextInt(4));
  }
  private void prepareElectroparen(ElectroProduct s) {
    fillSuperProp(s);
    fillElectroProp(s);
  }
  private <T extends WoodProduct> void fillWoodProp(T prod) {
    Random rnd = new Random(50);
    prod.setSize(25 + rnd.nextInt(5));
    prod.setColor(colorArr[rndArrInd()]);
  }
  private void prepareWoodProp(WoodProduct w) {
    fillSuperProp(w);
    fillWoodProp(w);
  }
  private <T extends PlumbingProduct> void fillPlumbProp(T prod) {
    Random rnd = new Random(50);
    prod.setCouplingDiam(8 + rnd.nextInt(14));
  }
  private void preparePlumbParent(PlumbingProduct p) {
    fillSuperProp(p);
    fillPlumbProp(p);
  }
  private Switch genSwitch(String n) {
    Random rnd = new Random(50);
    Switch outObj = new Switch(n);
    prepareElectroparen(outObj);
    outObj.setButton(rnd.nextInt( 3));
    return outObj;
  }
  private WashMachine genWashMach(String n) {
    WashMachine outObj = new WashMachine(n);
    preparePlumbParent(outObj);
    outObj.setModel(washMachModel[rndArrInd()]);
    return outObj;
  }

  private Valve genvalve(String n) {
    Random rnd = new Random(50);
    Valve outObj = new Valve(n);
    preparePlumbParent(outObj);
    outObj.setShowwer(rnd.nextBoolean());
    return outObj;
  }
  private Sink genSink(String n) {
    Sink outObj = new Sink(n);
    preparePlumbParent(outObj);
    outObj.setType(sinkType[rndArrInd()]);
    return outObj;
  }
  private Flooring genFlooring(String n) {
    Flooring outObj = new Flooring(n);
    prepareWoodProp(outObj);
    outObj.setTextur(floorText[rndArrInd()]);
    return outObj;
  }
  private Door genDoor(String n) {
    Door outObj = new Door(n);
    prepareWoodProp(outObj);
    outObj.setDoorType(dType[rndArrInd()]);
    return outObj;
  }
  public ArrayList<Product> fillStrore() {
    Random rnd = new Random(50);
    ArrayList<Product> outList = new ArrayList<Product>();
    for (int i = 1; i <=5; i++) {
      int nameSufix = 1 + rnd.nextInt(30);
      outList.add(genDoor("door " + nameSufix));
      outList.add(genFlooring("flooring " + nameSufix));
      outList.add(genSink("sink " + nameSufix));
      outList.add(genSwitch("switch " + nameSufix));
      outList.add(genvalve("valve " + nameSufix));
      outList.add(genWashMach("washingMachine " + nameSufix));
    }
    return outList;
  }



}
