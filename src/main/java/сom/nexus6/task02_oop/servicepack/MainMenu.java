package сom.nexus6.task02_oop.servicepack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import сom.nexus6.task02_oop.model.Publishible;
import сom.nexus6.task02_oop.model.Store;
import сom.nexus6.task02_oop.observer.Observable;
import сom.nexus6.task02_oop.observer.Report;

public class MainMenu {
  private final int FIRST_ITEM = 0;
  private final int LAST_ITEM = 3;
  private String[] menuItems = new String[3];

  public MainMenu() {
    menuItems[0] = "Show all products";
    menuItems[1] = "Show the best offer";
    menuItems[2] = "Show the cheapest products";
  }
  private void showMenu() {
    int menuCount = 0;
    for (String itm :menuItems) {
      ++menuCount;
      System.out.printf("%2d. %s%n", menuCount, itm);
    }
    System.out.print("Type 0 to exit the program : ");
  }
  public void getChoice(Store store) {
    showMenu();
    int choice = -1;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      String input = br.readLine();
      if (!input.matches("\\d")) {
        throw new IllegalArgumentException();
      } else if (Integer.valueOf(input) < FIRST_ITEM
                || Integer.valueOf(input) > LAST_ITEM) {
        throw new IllegalArgumentException();
      } else {
        choice = Integer.valueOf(input);
      }
    }
    catch (IOException e) {
      System.out.println("Read error");
    }
    catch (IllegalArgumentException exc) {
      System.out.println("Wrong menu item");
      getChoice(store);
    }
    switch (choice) {
      case 0:
        System.exit(0);
        break;
      case 1:
        Observable repGeneral = new Report();
        store.registerObs(repGeneral, new GeneralInfo());
        store.setState(true);
        break;
      case 2:
        Observable repBest = new Report();
        store.registerObs(repBest, new BestOffer());
        store.setState(true);
        break;
      case 3:
        Observable repMinPrice = new Report();
        store.registerObs(repMinPrice, new MinPrice());
        store.setState(true);
        break;
        default:
          break;
    }
  }
}
