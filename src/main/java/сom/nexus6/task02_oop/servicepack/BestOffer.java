package сom.nexus6.task02_oop.servicepack;

import java.util.ArrayList;
import сom.nexus6.task02_oop.prod.Product;

public class BestOffer implements Filterable {
  public ArrayList<Product> applyFilter(ArrayList<Product> data) {
    ArrayList<Product> rubish = new ArrayList<Product>();
    MinPrice minPrice = new MinPrice();

    for (Product pr : data) {
      if (!pr.getCountryOfOrg().equals("de")) {
        rubish.add(pr);
      }
    }
    data.removeAll(rubish);
    data = minPrice.applyFilter(data);
  return data;
  }
}
