package сom.nexus6.task02_oop.servicepack;

import java.util.ArrayList;
import сom.nexus6.task02_oop.prod.Product;

public class MinPrice implements Filterable {

  public ArrayList<Product> applyFilter(ArrayList<Product> data) {
    ArrayList<Product> rubish = new ArrayList<Product>();
    Product minPrice = data.get(0);
    for (int i = 0; i < data.size(); i++) {
      if (data.get(i).getPrice() > minPrice.getPrice()) {
        rubish.add(data.get(i));
      } else if (data.get(i).getPrice() < minPrice.getPrice()) {
        rubish.add(minPrice);
        minPrice = data.get(i);
      }
    }
    data.removeAll(rubish);
    data.trimToSize();
    return data;
  }

}
