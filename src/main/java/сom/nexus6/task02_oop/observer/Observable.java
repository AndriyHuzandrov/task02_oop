package сom.nexus6.task02_oop.observer;

import сom.nexus6.task02_oop.model.Publishible;

public interface Observable {
  void update(Publishible dataSource);

}
