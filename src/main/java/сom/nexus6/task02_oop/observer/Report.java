package сom.nexus6.task02_oop.observer;

import java.util.ArrayList;
import сom.nexus6.task02_oop.model.Publishible;
import сom.nexus6.task02_oop.prod.Product;

public class Report implements Observable {


  public  void update(Publishible store) {
    show(store.getOutputData());
    store.removeObs(this);
  }
  private void show(ArrayList<Product> data) {
    for (Product p : data) {
      System.out.printf("%nProduct name: %s%nPrice: %.2f%nMade in: %s%n",
                          p.getName(),
                          p.getPrice(),
                          p.getCountryOfOrg());
    }
  }
}
